package ca.bell.wt.apps.imsActiveRetry.test;


import ca.bell.wt.apps.imsActiveRetry.*;
import org.junit.runner.RunWith;
import com.eclipsesource.restfuse.HttpJUnitRunner;
import static org.junit.Assert.*;

import org.junit.Test;

@RunWith( HttpJUnitRunner.class )
public class ParseImpuVR_NotificationTest {

		@Test
        public void imeiAnyToAny() {
			
			String xml = "   <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + 
					"      <soapenv:Header>\r\n" + 
					"        <ns1:correlationHeader xmlns:ns1=\"http://new.webservice.namespace\" soapenv:mustUnderstand=\"0\">\r\n" + 
					"            <ns1:ServiceName>TestModifyDN</ns1:ServiceName>\r\n" + 
					"            <ns1:MsgId>538</ns1:MsgId>\r\n" + 
					"            <ns1:ConnId>8661-proxy-src-to-Test-ModifyDN-dest-pipe</ns1:ConnId>\r\n" + 
					"        </ns1:correlationHeader>\r\n" + 
					"      </soapenv:Header>\r\n" + 
					"      <soapenv:Body>\r\n" + 
					"        <ns1:notify xmlns:ns1=\"http://new.webservice.namespace\">\r\n" + 
					"            <ns1:directoryObject objectClass=\"top person organizationalPerson inetorgPerson wirelessPerson\" DN=\"min=9999000101,ou=022,o=mobility,c=ca\" operation=\"modify\">\r\n" + 
					"              <ns1:attribute name=\"impuVR\" modification=\"replace\">\r\n" + 
					"                  <ns1:beforeValue>sip:+14162892222@ims.bell.ca</ns1:beforeValue>\r\n" + 
					"                  <ns1:afterValue>sip:+14162892223@ims.bell.ca</ns1:afterValue>\r\n" + 
					"              </ns1:attribute>\r\n" + 
					"              <ns1:attribute name=\"mdn\" modification=\"none\">\r\n" + 
					"                  <ns1:currentValue>2891231234</ns1:currentValue>\r\n" + 
					"              </ns1:attribute>\r\n" + 
					"              <ns1:attribute name=\"accountType\" modification=\"none\">\r\n" + 
					"                  <ns1:currentValue>i</ns1:currentValue>\r\n" + 
					"              </ns1:attribute>\r\n" + 
					"              <ns1:attribute name=\"accountSubType\" modification=\"none\">\r\n" + 
					"                  <ns1:currentValue>j</ns1:currentValue>\r\n" + 
					"              </ns1:attribute>\r\n" + 
					"              <ns1:attribute name=\"ou\" modification=\"none\">\r\n" + 
					"                  <ns1:currentValue>022</ns1:currentValue>\r\n" + 
					"              </ns1:attribute>\r\n" + 
					" 	       <ns1:attribute name=\"subId2\" modification=\"none\">\r\n" + 
					"                  <ns1:currentValue>28673289990x0003BA0959BE1302199274997902-023X_wap2.bellmobility.ca</ns1:currentValue>\r\n" + 
					"               </ns1:attribute>\r\n" + 
					"           </ns1:directoryObject>\r\n" + 
					"        </ns1:notify>\r\n" + 
					"      </soapenv:Body>\r\n" + 
					"  </soapenv:Envelope>"
					;

        	
        	SdmNotificationEvent notification = new SdmNotificationEvent();
        	notification.parse(xml);
        	assertEquals("impuVR",notification.getNotificationType());
        	assertEquals("sip:+14162892222@ims.bell.ca", notification.getBeforeValue());
        	assertEquals("sip:+14162892223@ims.bell.ca", notification.getAfterValue());
        	assertEquals("2891231234", notification.getMdn());
        

        }
}
	
  
	
