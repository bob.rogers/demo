package ca.bell.wt.apps.imsActiveRetry.miscellaneous;

import java.util.Date;

public interface RuntimeInformationInterface
{

	public abstract void updateRunTimeInfo(String request);

	public abstract String getRunTimeInfoAsHtml();

	public abstract String getRunTimeInfoAsXml();

	public abstract Date getLastRequestTime();
	
	public abstract Date getStartTime();

	public abstract void setStartTime(Date startTime);

	public abstract int getRequestCounter();

	public abstract String getLastRequest();
	
	public abstract void setHistorySize(int size);

}