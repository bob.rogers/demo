package ca.bell.wt.apps.imsActiveRetry;


/*
 *	This is the main entry point for the ImsActiveRetry Code.
 * 
 *  This Method is activated when the SDM sends a SDM notification to ImsActiveRetry Application.
 * 
 * The SOAP Request looks like this:
     <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
      <soapenv:Header>
        <ns1:correlationHeader xmlns:ns1="http://new.webservice.namespace" soapenv:mustUnderstand="0">
            <ns1:ServiceName>TestModifyDN</ns1:ServiceName>
            <ns1:MsgId>538</ns1:MsgId>
            <ns1:ConnId>8661-proxy-src-to-Test-ModifyDN-dest-pipe</ns1:ConnId>
        </ns1:correlationHeader>
      </soapenv:Header>
      <soapenv:Body>
        <ns1:notify xmlns:ns1="http://new.webservice.namespace">
            <ns1:directoryObject objectClass="top person organizationalPerson inetorgPerson wirelessPerson" DN="min=9999000101,ou=022,o=mobility,c=ca" operation="modify">
              <ns1:attribute name="impuVR" modification="replace">
                  <ns1:beforeValue>sip:+14162892222@ims.bell.ca</ns1:beforeValue>
                  <ns1:afterValue>sip:+14162892223@ims.bell.ca</ns1:afterValue>
              </ns1:attribute>
              <ns1:attribute name="mdn" modification="none">
                  <ns1:currentValue>4167709740</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountType" modification="none">
                  <ns1:currentValue>i</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountSubType" modification="none">
                  <ns1:currentValue>j</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="ou" modification="none">
                  <ns1:currentValue>022</ns1:currentValue>
              </ns1:attribute>
 	       <ns1:attribute name="subId2" modification="none">
                  <ns1:currentValue>28673289990x0003BA0959BE1302199274997902-023X_wap2.bellmobility.ca</ns1:currentValue>
               </ns1:attribute>
           </ns1:directoryObject>
        </ns1:notify>
      </soapenv:Body>
  </soapenv:Envelope>
 * 
 * 
 * 	
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STRawGroupDir;

import ca.bell.wt.ServiceEnabler.util.ResourceLoader;
import ca.bell.wt.apps.imsActiveRetry.miscellaneous.Audit;
import ca.bell.wt.apps.imsActiveRetry.miscellaneous.RuntimeInformation;
import ca.bell.wt.apps.imsActiveRetry.miscellaneous.RuntimeInformationInterface;

import javax.enterprise.event.Event;
import javax.inject.Inject;

@Path("/")
public class ImsActiveRetryHandler 
{
	@Inject
	Event<SdmNotificationEvent> pickUpEvent;   // used for asynchronous processing of the OTA request

	private static final Logger logger = Logger.getLogger(ImsActiveRetryHandler.class.getName());
	private static RuntimeInformationInterface runtimeInfo; // to hold statistics
	private static Audit audit; // for auditing messages
	private static String imeiBlankToAny;   // flag to indicate on what imei changes to process
	private static String anyToBlank;   	// flag to indicate if a afterValue = blank is processed or not. 
    private static String stringTemplatesDirectory;

	static
	{
		// need to initialize the Runtime and Audit objects, but only once, hence that's why they are in the static block

		imeiBlankToAny = ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("imeiBlankToAny");   // flag to indicate on what imei changes to process
		if (imeiBlankToAny.equalsIgnoreCase(""))	// default to true if not defined in property file
			imeiBlankToAny = "true";

		anyToBlank = ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("anyToBlank");   // flag to indicate if an afterValue of blank is processed. 
		if (anyToBlank.equalsIgnoreCase(""))	// default if not defined in property file
			anyToBlank = "false";

		stringTemplatesDirectory = ResourceLoader.getAsProperties("ota.properties").getProperty("stringTemplatesDirectory"); 
		if (stringTemplatesDirectory.equalsIgnoreCase(""))	
			stringTemplatesDirectory = "StringTemplates";		// default if not defined in properties file.
        
		
		runtimeInfo = new RuntimeInformation();
		runtimeInfo.setStartTime(new Date()); // set the start time of the application

		audit = new Audit();
	}  


	/*
	 * Main entry point for application
	 * Receives the SDM notifications
	 * 
	 *  Example url usage:   http://localhost:8080/imsActiveRetry/sdmNotification
	 *  
	 */

	@POST
	@Path("/sdmNotification")
	@Produces("text/html; charset=UTF-8")
	public Response sdmNotification(@Context HttpServletRequest request) 
	{
		boolean processSdmNotification = false;
		String sdmResponse = "";
		String mdn = "";
		try 
		{
			String httpPayload = ImsActiveRetryHandler.getBody(request);
			SdmNotificationEvent sdmNotification = new SdmNotificationEvent();
            
			//--------------------------------------------------------------------------
			// Parse the Soap SDM Notification and check for malformed SDM notification 
			//--------------------------------------------------------------------------
			if (!sdmNotification.parse(httpPayload))
			{			
				// malformed SDM Notification, log error
				logger.error("Recieved malformed SDM notification,  request = " + httpPayload); 
                audit.log("Recieved malformed SDM notification, check the debug logs for more details.");
			}
			else		// well formed SDM Notification, continue processing 
			{
				mdn = sdmNotification.getMdn();

				logger.info("Recieved SDM Notification Request: for mdn = " + mdn); 
				logger.debug("Recieved SDM Notification Request: for mdn = " + mdn + ": " + sdmNotification.toString()); 
				logger.trace("Soap SDM Notification Request: for mdn = " + mdn + ": " + sdmNotification.getNotificationXml()); 

				//--------------------------------------------------------------------------
				// determine if this notification should be processes or skipped. 
				//--------------------------------------------------------------------------

				String notificationChangeType = sdmNotification.getNotificationType();	 // imei or impuRV	
				processSdmNotification = filterOutNotifications(sdmNotification, mdn); 	// filter out (ie skip) sdm notifications that don't meet the criteria

				// *** process the Notification by issuing an update request to the OTA Platform ***
				if (processSdmNotification)
				{
					logger.info("Sdm Notification being processed for mdn = " + mdn + " due to a " + notificationChangeType + " change."); 
					runtimeInfo.updateRunTimeInfo("mdn = " + mdn + ", msgId = " + sdmNotification.msgId + ", changeType = " + notificationChangeType + 
											", beforeValue = " + sdmNotification.beforeValue + ", afterValue = " + sdmNotification.afterValue + ", ou = " + sdmNotification.ou +  
											", message = request sent to OTA platform."  );

					// asynchronously process the OTA request.  Note this is a non-blocking call!
					// this fire event is handled by the ProcessOta.notificationListener() method.
					pickUpEvent.fire(sdmNotification);		
				}
			}

			//-----------------------------------------------------------------------------------
			// Return a SOAP Response to the SDM
			// Note: we always return a valid response to the SDM even though we may or may not 
			// send a request to the OTA platform.  This is done to prevent SDM retries
			//-------------------------------------------------------------------------------------
			sdmResponse = generateSdmSoapResponse(sdmNotification.getServiceName(), sdmNotification.getMsgId(), sdmNotification.getConnId());
			logger.info("Sending SDM Response  for mdn = " + mdn);
			logger.trace("Sending this SDM Soap Response  for mdn = " + mdn + " \n" + sdmResponse);
			return Response.status(Status.OK).entity(sdmResponse).build();			

		} 
		catch (IOException e) 
		{
			logger.warn(e.getMessage());
			return Response.status(Status.SERVICE_UNAVAILABLE).build();			
		} catch (Exception e) {
			logger.warn(e.getMessage());
			return  Response.status(Status.SERVICE_UNAVAILABLE).build() ;			
		}

	}

	/*
	 * This method determines if the SDM notification should be skipped or processed 
	 * Returns true: if notification should be processed
	 * Returns false: if notification should be skipped
	 */
	public  static boolean filterOutNotifications(SdmNotificationEvent sdmNotification, String mdn)
	{
		boolean result = false; 

		String notificationChangeType = sdmNotification.getNotificationType();	// imei or impuVR

		// for both imei and impuVR, skip if afterValue = blank and configuration flag (anyToBlank) is set
		if (anyToBlank.equalsIgnoreCase("false") && sdmNotification.afterValue.equalsIgnoreCase(""))
		{
			result = false;
			logger.info("Sdm Notification ignored for mdn = " + mdn + " because the afterValue was blank and anyToBlank configuration parameter was set to false."); 
			audit.log(mdn, sdmNotification.getMsgId(), notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType, 
					sdmNotification.accountSubType, "", "", "", 0, "", "Sdm Notification ignored because afterValue was blank and anyToBlank configuration parameter was set to false.");
			runtimeInfo.updateRunTimeInfo("mdn = " + mdn +  ", msgId = " + sdmNotification.msgId + ", changeType = " + notificationChangeType + ", beforeValue = " + sdmNotification.beforeValue + ", afterValue = " + sdmNotification.afterValue + 
					", ou = " + sdmNotification.ou + ", message = Sdm Notification ignored for mdn  " + mdn + " because the afterValue was blank and anyToBlank configuration parameter was set to false.");
		}
		
        // always process impuVR changes
		else if (notificationChangeType.equalsIgnoreCase("impuVR")) 
		{
			result = true;
		}
		
		// skip if imei any to any condition
		else  if (notificationChangeType.equalsIgnoreCase("imei") && 		
			     !sdmNotification.beforeValue.equalsIgnoreCase("") && 
			     !sdmNotification.afterValue.equalsIgnoreCase(""))
		{ 
				result = false; 	// ignore imei AnyToAny
				logger.info("Sdm Notification ignored for mdn = " + mdn + " because the imei before and after value was NOT blank. (ie imei Any2ToAny changes should be ignored."); 
				audit.log(mdn, sdmNotification.getMsgId(), notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType, 
				sdmNotification.accountSubType, "", "", "", 0, "", "Sdm Notification ignored because imei before and after valuse was NOT blank.");
				runtimeInfo.updateRunTimeInfo("mdn = " + mdn +  ", msgId = " + sdmNotification.msgId + ", changeType = " + notificationChangeType + ", beforeValue = " + sdmNotification.beforeValue + 
						", afterValue = " + sdmNotification.afterValue + ", ou = " + sdmNotification.ou + ", message = Sdm Notification ignored for mdn  " + mdn + "because imei before and after valuse was NOT blank.");
		}

		// skip imei any to blank condition and if configuration flag is set
		else  if (notificationChangeType.equalsIgnoreCase("imei") && 		
			     sdmNotification.beforeValue.equalsIgnoreCase("") && 
			     !sdmNotification.afterValue.equalsIgnoreCase(""))
		{
				if (imeiBlankToAny.equalsIgnoreCase("false"))		// process based on a configuration flag
					result = false; 	// ignore blankToAny
				else
					result = true;		// process blankToAny
		}

		else	// neither a imei change or an impuVR change ==> ignore this request
		{
			result = false; 	
			logger.info("Sdm Notification ignored for mdn = " + mdn + " because the change was not an impuVR or imei change."); 
			audit.log(mdn, sdmNotification.getMsgId(), notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType, 
					sdmNotification.accountSubType, "", "", "", 0, "", "Sdm Notification ignored because change was not an impuVR or imei change.");
			runtimeInfo.updateRunTimeInfo("mdn = " + mdn +  ", msgId = " + sdmNotification.msgId + ", changeType = " + notificationChangeType + ", beforeValue = " + sdmNotification.beforeValue + ", afterValue = " + sdmNotification.afterValue + 
					", ou = " + sdmNotification.ou + ", message = Sdm Notification ignored for mdn  " + mdn + " because the change was not an impuVR or imei change.");

		}
		return result;
	}




	/*-------------------------------------------------------------------------------------
	 * getBody()
	 * 	Simple function to extract the payload (ie body) of an SOAP http request
	 * 	Return the body as a string.
	 * ------------------------------------------------------------------------------------
	 */
	public static String getBody(HttpServletRequest request) throws IOException 
	{

		String body = null;
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;

		try {
			InputStream inputStream = request.getInputStream();
			if (inputStream != null) {
				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;
				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}

		body = stringBuilder.toString();
		return body;
	}


	/*
	 *  This class is used to generate the SDM soap response  using string templates
	 *  
	 * 	An example SDM Soap Notification Response looks like this
	 * 
			 <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
				   <soapenv:Header>
				      <ns1:correlationHeader xmlns:ns1="http://new.webservice.namespace">
				         <ns1:ServiceName>?serviceName?</ns1:ServiceName>
				         <ns1:MsgId>?msgId?538</ns1:MsgId>
				         <ns1:ConnId>?connId?</ns1:ConnId>
				      </ns1:correlationHeader>
				   </soapenv:Header>
				   <soapenv:Body>
				      <ns1:NotificationResponse xmlns:ns1="http://new.webservice.namespace"/>
				   </soapenv:Body>
				</soapenv:Envelope>
	 */
	

	public String generateSdmSoapResponse(String serviceName, String msgId, String connId)
	{
		STGroup group = new STRawGroupDir(ImsActiveRetryHandler.stringTemplatesDirectory, '$', '$');
        logger.trace("Using stringTemplateDirectory = " + ImsActiveRetryHandler.stringTemplatesDirectory);
		ST sdmResponseTemplate = group.getInstanceOf("SDM_SoapResponse");
		sdmResponseTemplate.add("serviceName", serviceName);
		sdmResponseTemplate.add("msgId", msgId);
		sdmResponseTemplate.add("connId", connId);
		String result = sdmResponseTemplate.render();

		return result;
	}






	/*
	 * These are used by the OAS Admin Gui to return the runtime information for this application.
	 */
	
	@GET
	@Path("/rt")
	@Produces("text/html")
	public String runTimeAsHtml(@Context HttpServletRequest request)
	{
		return runtimeInfo.getRunTimeInfoAsHtml();
	}

	@GET
	@Path("/rtxml")
	@Produces("application/xml")
	public String runTimeAsXml(@Context HttpServletRequest request)
	{
		return runtimeInfo.getRunTimeInfoAsXml();
	}		


}

