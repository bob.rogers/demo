package ca.bell.wt.apps.imsActiveRetry.miscellaneous;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Produces;




/**
 * Hold the Statistics on an Application 
 *  
 * Example Usage  
 * 
 *	    private static RuntimeInformationInterface runtimeInfo;	// to hold statistics
 * 		DeviceDetector.runtimeInfo = new RuntimeInformation(); 	// initialize
 *		.... 
 *		runtimeInfo.updateRunTimeInfo(lastRequest); 			// update the runtime info
 *		...
 *		System.out.println(runtimeInfo.getRunTimeInfoAsHtml());	// print the runtime info in html
 *		System.out.println(runtimeInfo.getRunTimeInfoAsXml());	// print the runtime info in xml
 *
 *
 *
 */

public class RuntimeInformation implements RuntimeInformationInterface 
{
	private static final  int HISTORY_SIZE = 50;	// default # of historical requests to keep.
	
	private Date startTime;			// application start time (or the time it processed it's 1st request)
	private Date lastRequestTime;	// when the last request was processed.
	private int  requestCounter;	// # of requests processed to date.
	private String lastRequest;		// the last request processed.
	private ArrayList<String> requestHistory;	// the last X requests processed
	private int historySize = HISTORY_SIZE;

	
	public RuntimeInformation()
	{
		super();
		Date now = new Date();
		startTime = now;
		lastRequestTime = now;
		requestCounter = 0;
		lastRequest = "";
		requestHistory = new ArrayList<String>();
	}
	
	public void updateRunTimeInfo(String request)
	{
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss SSS");
		Date now = new Date();
 		String nowAsString = sdf.format(now);

		this.setLastRequestTime(now);
		this.setRequestCounter(this.requestCounter + 1);
		this.setLastRequest(nowAsString + ": " + request);
		this.requestHistory.add(nowAsString + ": " + request);
		if (this.requestHistory.size() > historySize)
			this.requestHistory.remove(0);		// trim the requrestHistory by removing the 1st element
		
	}

		
    
	
	
	

	@Produces("text/html")
	public String getRunTimeInfoAsHtml()
	{
		
		// formulate the request history into a string
		StringBuffer reqHis = new StringBuffer();
		reqHis.append("<OL>");
		for (String req : requestHistory) 
			reqHis.append("<LI> " + req.toString() + "</LI>");
		reqHis.append("</OL>");
    	   
		return(
			"<html><body>" + 
			"<br><b> Request Count  : </b>" +  this.getRequestCounter() + "</br>" +
			"<br><b> Start Time     : </b>" + this.getStartTime().toString() + "</br>" +
			"<br><b> Last Time      : </b>" + this.getLastRequestTime().toString() + "</br>" +
			"<br><b> Last Request   : </b>" + this.getLastRequest() + "</br>" +
			"<br><b> Request History: </b>" + reqHis + "</br>" +
			"</body></html>"
			);	
	}
	

	public String getRunTimeInfoAsXml()
	{
		// formulate the request history into a string
		StringBuffer reqHis = new StringBuffer();
		for (String req : requestHistory) 
			reqHis.append("<request>" + req.toString() + "</request>");

		return("<RunTimeInfo>" + 
			"<StartTime>" + this.getStartTime().toString() + "</StartTime>" +  
			"<LastRequestTime>" + this.getLastRequestTime().toString() + "</LastRequestTime>" + 
			"<RequestCount>" +  this.getRequestCounter() + "</RequestCount>" + 
			"<LastRequest>" + this.getLastRequest() + "</LastRequest>" +
			"<RequestHistory>" + reqHis + "</RequestHistory>" + 
			"</RunTimeInfo>"
			);	
	}
	
	
	public Date getLastRequestTime()
	{
		return lastRequestTime;
	}

	public void setLastRequestTime(Date lastRequestTime)
	{
		this.lastRequestTime = lastRequestTime;
	}

	
	public Date getStartTime()
	{
		return startTime;
	}

	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}


	public int getRequestCounter()
	{
		return requestCounter;
	}

	public String getLastRequest()
	{
		return lastRequest;
	}

	private void setLastRequest(String lastRequest)
	{
		this.lastRequest = lastRequest;
	}


	private void setRequestCounter(int requestCounter)
	{
		this.requestCounter = requestCounter;
	}

	public  void setHistorySize(int size)
	{
		this.historySize = size;
	}
}
