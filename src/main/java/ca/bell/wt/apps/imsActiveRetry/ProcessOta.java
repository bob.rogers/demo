package ca.bell.wt.apps.imsActiveRetry;


import ca.bell.wt.ServiceEnabler.ota.OtaService;
import ca.bell.wt.ServiceEnabler.ota.OtaUpdateSubscriptionResult;
import ca.bell.wt.apps.imsActiveRetry.miscellaneous.Audit;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import org.apache.log4j.Logger;

/*
 * This class issues the OTA request asynchronously .
 * Note the annotations @Stateless and @Asynchronous, these are very important.
 * 
 * The purpose of this class is to issue a SOAP request to the OTA platform 
 * and handle the SOAP response.
 */


@Stateless
@Asynchronous
public class ProcessOta
{

	private static final Logger logger = Logger.getLogger(ProcessOta.class.getName());
	private static Audit audit; // for auditing messages

	static
	{
		audit = new Audit();
	}  
    

	/*
	 * This method gets triggered automatically by the call in 
	 * ImsActiveRetryHandler.sdmNotification().pickUpEvent.fire() call
	 */

	public void notificationListener(@Observes SdmNotificationEvent sdmNotification)
	{

		OtaService otaService;					// Service Enabler to allow us to communicate with the OTA Platform.
		String notificationChangeType = ""; 	// imei or imprRV  change
		String otaSoapRequest = ""; 
		String mdn = sdmNotification.getMdn();	// mdn 
		OtaUpdateSubscriptionResult otaResult = null;	// Result of the call to the OTA Platform.
            
		try
		{
			otaService = new OtaService("ota.properties");
			notificationChangeType = sdmNotification.getNotificationType();		// imei or impuRV
			otaSoapRequest = otaService.generateOtaUpdateSubscriptionSoapRequest(mdn); // generate the soap request to OTA

			logger.info("Sending OTA Request for mdn = " + mdn);
			logger.debug("Sending OTA Request for mdn = " + mdn + ", Request: \n" + sdmNotification.toString());

            // send to ota update request to the ota platform
			otaResult = otaService.issueOtaUpdateSubscriptionSoapRequest(otaSoapRequest);

			// log the result to the debug, audit, and runtime logs
			logger.info("OTA Response for mdn = " + mdn + ", code = " + otaResult.getHttpResponseCode() + ", message = " + otaResult.getHttpResponseMessage());
			logger.debug("OTA Response for mdn = " + mdn + ": " + otaResult);
			
            
			if (otaResult.getHttpResponseCode() == Constants.httpOk)
                // success
				audit.log(mdn, sdmNotification.msgId, notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType,
					sdmNotification.accountSubType, otaResult.getOtaStatus(), otaResult.getOtaRequestId(), otaResult.getOtaRequestStatus(),
					otaResult.getHttpResponseCode(), otaResult.getHttpResponseMessage(), "Successfully updated OTA.");
			else
                // failure
				audit.log(mdn, sdmNotification.msgId, notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType,
					sdmNotification.accountSubType, otaResult.getOtaStatus(), otaResult.getOtaRequestId(), otaResult.getOtaRequestStatus(),
					otaResult.getHttpResponseCode(), otaResult.getHttpResponseMessage(), "Failure in updating OTA.");
				

		}
		catch (Exception e)
		{
            logger.warn(e.getMessage());
            if (otaResult != null && sdmNotification != null)
            	audit.log(mdn, sdmNotification.msgId, notificationChangeType, sdmNotification.beforeValue, sdmNotification.afterValue, sdmNotification.ou, sdmNotification.accountType,
					sdmNotification.accountSubType, otaResult.getOtaStatus(), otaResult.getOtaRequestId(), otaResult.getOtaRequestStatus(),
					otaResult.getHttpResponseCode(), otaResult.getHttpResponseMessage(), "Failure in updating OTA.");
		}

	}

}
