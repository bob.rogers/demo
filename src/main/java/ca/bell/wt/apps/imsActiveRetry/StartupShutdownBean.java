package ca.bell.wt.apps.imsActiveRetry;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import ca.bell.wt.ServiceEnabler.util.ResourceLoader;
import ca.bell.wt.apps.imsActiveRetry.miscellaneous.Audit;

/*
 * Simple class to log some messages at deploy and undeploy time.
 */

@Startup
@Singleton
public class StartupShutdownBean
{
	private static final Logger logger = Logger.getLogger(StartupShutdownBean.class.getName());

	@PostConstruct
	private void startup() 
	{
        logger.info("");
        logger.info("!!! Startup - ImsActiveRetry Application Starting !!!");
		logger.info("Starting with configuration parameters:  imeiBlankToAny = " + ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("imeiBlankToAny") 
				+ ", anyToBlank = " + ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("anyToBlank") 
				+ ", otaHealthCheckMdn = " + ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("otaHealthCheckMdn") +
				", stringTemplatesDirectory = " + ResourceLoader.getAsProperties("ota.properties").getProperty("stringTemplatesDirectory"));
		
	}
	
	@PreDestroy
	private void shutdown() 
	{ 
		
        logger.info("!!! Shutdown - ImsActiveRetry Application Shutting Down !!!");
        logger.info("");
		
	}


}