package ca.bell.wt.apps.imsActiveRetry.miscellaneous;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STRawGroupDir;

import ca.bell.wt.ServiceEnabler.ota.OtaService;
import ca.bell.wt.ServiceEnabler.ota.OtaUpdateSubscriptionResult;
import ca.bell.wt.ServiceEnabler.util.ResourceLoader;

@Path("/")
public class HealthCheck
{
	private static final Logger logger = Logger.getLogger(HealthCheck.class.getName());

	
	@GET
	@Path("/ping")
	public Response ping()
	{
		boolean allGood = true;
		String result = "";
		OtaUpdateSubscriptionResult otaResult = null;

		try
		{
			/*
			 * Check the health of the ota platform
			 */
			
			OtaService otaService;
			otaService = new OtaService("ota.properties");
			String mdnHealthCheck = ResourceLoader.getAsProperties("imsActiveRetry.properties").getProperty("otaHealthCheckMdn");
			
			String otaSoapRequest = otaService.generateOtaUpdateSubscriptionSoapRequest(mdnHealthCheck);
			otaResult = otaService.issueOtaUpdateSubscriptionSoapRequest(otaSoapRequest);

			if (otaResult.getHttpResponseCode() != 200)
				allGood = false;

		}
		catch (Exception e)
		{
			allGood = false;
			result = "imsActiveRetry=Not Healthy {ota=" + e.getMessage()+ "}";
			logger.info(result);
			return Response.status(Status.SERVICE_UNAVAILABLE).entity(result).build();
		}

		if (allGood == true) // at this point, all tests have passed, therefore the application is good
		{
			result = "imsActiveRetry=Healthy {ota=OK}"; 
			logger.info(result);
			return Response.status(Status.OK).entity(result).build();
		}
		else	// allGood == false
		{
			result = "imsActiveRetry=Not Healthy {ota=" + otaResult.getHttpResponseMessage() + "}";
			logger.info(result);
			return Response.status(Status.SERVICE_UNAVAILABLE).entity(result).build();
		}
	}
    
	

	
	@GET
	@Path("/test")
	public Response test()
	{
		String result = "empty for start";
		try
		{
			// build the Soap call using a template
			
	        // STGroup group = new STRawGroupDir("StringTemplates", '$', '$');	//  NOT working on oasx2 but is working on oasx1
	        // STGroup group = new STRawGroupDir("/tmp/StringTemplates", '$', '$');	// working !!!
	       // STGroup group = new STRawGroupDir("/JBoss/jboss-eap-6.1/modules/ca/bell/wt/apps/imsActiveRetry/main/StringTemplates", '$', '$');	// working !!!
	        // STGroup group = new STRawGroupDir("/StringTemplates", '$', '$');	//  NOT working :(
	        // STGroup group = new STRawGroupDir("main/StringTemplates", '$', '$');	//  NOT working :(
	         // STGroup group = new STRawGroupDir("ca/bell/wt/apps/imsActiveRetry/main/StringTemplates", '$', '$');	// NOT working 
            
            String dir = ResourceLoader.getAsProperties("ota.properties").getProperty("stringTemplatesDirectory"); // working !!! 
	        STGroup group = new STRawGroupDir(dir, '$', '$');	// working !!!
			
	        ST otaRequestTemplate = group.getInstanceOf("OPM_UpdateSubscriberSoapRequest");
	        otaRequestTemplate.add("otaUserName", "bob");
	        otaRequestTemplate.add("otaPassword", "rogers");
	        otaRequestTemplate.add("mdn", "4165559999");
	        result  = otaRequestTemplate.render();
            logger.debug("result of test = " + result);
	        
		} catch (Exception e)
		{
			logger.error(e.getMessage(), e);
		}

		return Response.status(Status.SERVICE_UNAVAILABLE).entity(result).build();
		
	}
	
	
	


}
