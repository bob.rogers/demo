/*
 * Audit.java
 * Used to create and log audit messages.
 * Created on March 15, 2005, 12:53 PM
 */

package ca.bell.wt.apps.imsActiveRetry.miscellaneous;

 
import org.apache.log4j.*;
import org.apache.commons.lang.*;


/*
 * This is the Audit class for this application.
 * It logs all activity of the application.
 * It uses log4j as the logging engine which can log audit messages
 * to flat files, to the console, to a database, and more if desired.
 * 
 * This class needs to modified for each application as each application has it's 
 * own unique auditing information.  Below is an example log4j.xml file that logs
 * the audit message to a flat file and to a database (mySql in this case).
 * 
 */

/* 
		Below is a sample log4 configuration for the OPTOUT Audit
 <!-- ===================================== -->
 <!-- OptOut Auditing into Flat File   -->
 <!-- ===================================== -->
    <!-- A size based file rolling appender for auditing only -->
		<appender name="IMS_ACTIVE_RETRY_AUDIT_FF" class="org.apache.log4j.DailyRollingFileAppender">
			<errorHandler class="org.apache.log4j.helpers.OnlyOnceErrorHandler"/>
			<param name="File" value="${jboss.server.log.dir}/imsActiveRetry/imsActiveRetry_audit.log"/>
			<param name="DatePattern" value=".yyyy-MM-dd"/>
			<param name="Threshold" value="INFO"/>
			<layout class="org.apache.log4j.PatternLayout">
				<param name="ConversionPattern" value="%d| ip=%X{IP}| notifyType=%X{notifyType}| beforeValue=%X{beforeValue}| afterValue=%X{afterValue}| ou=%X{ou}| accountType=%X{accountType}| accountSubType=%X{accountSubType}| otaStatus=%X{otaStatus}| otaRequestId=%X{otaRequestId}| otaRequestStatus=%X{otaRequestStatus}| otaHttpResponseCode=%X{otaHttpResponseCode}| otaHttpResponseMessage=%X{otaHttpResponseMessage}|  Message=%m%n"/>
				</layout>
		</appender>

  <category name="AUDIT.ca.bell.wt.apps.imsActiveRetry" additivity="false">
    <priority  value="INFO" />
    <appender-ref ref="IMS_ACTIVE_RETRY_AUDIT_FF"/>
   </category>

*/

public class Audit
{
  
  static final String AUDIT = "AUDIT.";					// log4j category (prepend it to your class path)
  static final Logger logger = Logger.getLogger(AUDIT + Audit.class.getName());	// log4j logger

  // The following fields are logged
  String mdn;
  String notifyType;		// imei or impuVR
  String msgId;				// from the sdm notification
  String beforeValue;
  String afterValue;
  String ou;
  String accountType;
  String accountSubType;
  String otaStatus;
  String otaRequestId;
  String otaRequestStatus;
  int otaHttpResponseCode;
  String otaHttpResponseMessage;
  String optionalMessage;
  
  
  public Audit()
  {
	  mdn = "";
	  msgId = "";
	  notifyType = "";		// imei or impuVR
	  beforeValue = "";
	  afterValue = "";
	  ou = "";
	  accountType = "";
	  accountSubType = "";
	  otaStatus = "";
	  otaRequestId = "";
	  otaRequestStatus = "";
	  otaHttpResponseCode = 0;
	  otaHttpResponseMessage = "";
      
  }


  
  public void log( String mdn, String msgId, String notifyType, String beforeValue, String afterValue, String ou, String accountType, String accountSubType, 
  				String otaStatus, String otaRequestId, String otaRequestStatus, int otaHttpResponseCode, String otaHttpResponseMessage, String optionalMessage)
  {
	  this.mdn = mdn;
	  this.msgId = msgId;
  	  this.notifyType =	notifyType;
  	  this.beforeValue = beforeValue;
  	  this.afterValue = afterValue;
  	  this.ou = ou;
  	  this.accountType = accountType;
  	  this.accountSubType = accountSubType;
  	  this.otaStatus = otaStatus;
  	  this.otaRequestId = otaRequestId;
  	  this.otaRequestStatus = otaRequestStatus;
  	  this.otaHttpResponseCode = otaHttpResponseCode;
  	  this.otaHttpResponseMessage = otaHttpResponseMessage;
  	  this.optionalMessage = optionalMessage;
	  
	  log();
  }
 
  public void log(String optionalMessage)
  {
	  
	  mdn = "";
	  msgId = "";
	  notifyType = "";		// imei or impuVR
	  beforeValue = "";
	  afterValue = "";
	  ou = "";
	  accountType = "";
	  accountSubType = "";
	  otaStatus = "";
	  otaRequestId = "";
	  otaRequestStatus = "";
	  otaHttpResponseCode = 0;
	  otaHttpResponseMessage = "";
  	  this.optionalMessage = optionalMessage;
  	  log();
	  
  }
  
  private void log()
  {
    MDC.put("mdn", StringUtils.defaultString(this.mdn));	
    MDC.put("msgId", StringUtils.defaultString(this.msgId));	
    MDC.put("notifyType", StringUtils.defaultString(this.notifyType));	
    MDC.put("beforeValue", StringUtils.defaultString(this.beforeValue));	
    MDC.put("afterValue", StringUtils.defaultString(this.afterValue));	
    MDC.put("ou", StringUtils.defaultString(this.ou));	
    MDC.put("accountType", StringUtils.defaultString(this.accountType));	
    MDC.put("accountSubType", StringUtils.defaultString(this.accountSubType));	
    MDC.put("otaStatus", StringUtils.defaultString(this.otaStatus));	
    MDC.put("otaRequestId", StringUtils.defaultString(this.otaRequestId));	
    MDC.put("otaRequestStatus", StringUtils.defaultString(this.otaRequestStatus));	
    MDC.put("otaHttpResponseCode", StringUtils.defaultString(Integer.toString(this.otaHttpResponseCode)));	
    MDC.put("otaHttpResponseMessage", StringUtils.defaultString(this.otaHttpResponseMessage));	
    MDC.put("optionalMessage", StringUtils.defaultString(this.optionalMessage));	
    
    logger.info(StringUtils.defaultString(optionalMessage));   	// log the message to log4j
  }

}
