package ca.bell.wt.apps.imsActiveRetry;



import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/*
 *	This class is used to parse SDM Change Notifications
 *	A notification comes in 2 flavours
 *		1. IMEI Notification	
 *		2. IMPU Notification 
 * 
 * They are basically identical in structure.
 * 
 * Below is an example of a IMPU SDM Change Notification
 * 
 * 
   <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
      <soapenv:Header>
        <ns1:correlationHeader xmlns:ns1="http://new.webservice.namespace" soapenv:mustUnderstand="0">
            <ns1:ServiceName>TestModifyDN</ns1:ServiceName>
            <ns1:MsgId>538</ns1:MsgId>
            <ns1:ConnId>8661-proxy-src-to-Test-ModifyDN-dest-pipe</ns1:ConnId>
        </ns1:correlationHeader>
      </soapenv:Header>
      <soapenv:Body>
        <ns1:notify xmlns:ns1="http://new.webservice.namespace">
            <ns1:directoryObject objectClass="top person organizationalPerson inetorgPerson wirelessPerson" DN="min=9999000101,ou=022,o=mobility,c=ca" operation="modify">
              <ns1:attribute name="impuVR" modification="replace">
                  <ns1:beforeValue>sip:+14162892222@ims.bell.ca</ns1:beforeValue>
                  <ns1:afterValue>sip:+14162892223@ims.bell.ca</ns1:afterValue>
              </ns1:attribute>
              <ns1:attribute name="mdn" modification="none">
                  <ns1:currentValue>2891231234</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountType" modification="none">
                  <ns1:currentValue>i</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountSubType" modification="none">
                  <ns1:currentValue>j</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="ou" modification="none">
                  <ns1:currentValue>022</ns1:currentValue>
              </ns1:attribute>
 	       <ns1:attribute name="subId2" modification="none">
                  <ns1:currentValue>28673289990x0003BA0959BE1302199274997902-023X_wap2.bellmobility.ca</ns1:currentValue>
               </ns1:attribute>
           </ns1:directoryObject>
        </ns1:notify>
      </soapenv:Body>
  </soapenv:Envelope>
 * 
 * 
 * 
 * Below is an example of a IMEI SDM Change Notification
 * 
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
      <soapenv:Header>
        <ns1:correlationHeader xmlns:ns1="http://new.webservice.namespace" soapenv:mustUnderstand="0">
            <ns1:ServiceName>TestModifyDN</ns1:ServiceName>
            <ns1:MsgId>538</ns1:MsgId>
            <ns1:ConnId>8661-proxy-src-to-Test-ModifyDN-dest-pipe</ns1:ConnId>
        </ns1:correlationHeader>
      </soapenv:Header>
      <soapenv:Body>
        <ns1:notify xmlns:ns1="http://new.webservice.namespace">
            <ns1:directoryObject objectClass="top person organizationalPerson inetorgPerson wirelessPerson" DN="min=9999000101,ou=022,o=mobility,c=ca" operation="modify">
              <ns1:attribute name="imei" modification="replace">
                  <ns1:beforeValue></ns1:beforeValue>
                  <ns1:afterValue>444555666</ns1:afterValue>
              </ns1:attribute>
              <ns1:attribute name="mdn" modification="none">
                  <ns1:currentValue>2891231234</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountType" modification="none">
                  <ns1:currentValue>i</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="accountSubType" modification="none">
                  <ns1:currentValue>j</ns1:currentValue>
              </ns1:attribute>
              <ns1:attribute name="ou" modification="none">
                  <ns1:currentValue>022</ns1:currentValue>
              </ns1:attribute>
 	       <ns1:attribute name="subId2" modification="none">
                  <ns1:currentValue>28673289990x0003BA0959BE1302199274997902-023X_wap2.bellmobility.ca</ns1:currentValue>
               </ns1:attribute>
           </ns1:directoryObject>
        </ns1:notify>
      </soapenv:Body>
  </soapenv:Envelope>
 * 
 * 
 * 
 */

public class SdmNotificationEvent
{

	String notificationType;  // imei or impuVR
	String beforeValue;
	String afterValue;
	String mdn;
	String accountType;
	String accountSubType;
	String ou;
	String subId2;
	String serviceName;
    String msgId;
    String connId;
    String notificationXml;
	

	private static final Logger logger = Logger.getLogger(SdmNotificationEvent.class.getName());

	// default constructor
	public SdmNotificationEvent()
	{
		notificationType = "";
		beforeValue = "";
		afterValue = "";
		mdn = "";
		accountType = "";
		accountSubType = "";
		ou = "";
		subId2 = "";
		serviceName = "";
    	msgId = "";
    	connId = "";
    	notificationXml = "";
	}
	
	public SdmNotificationEvent(String sdmNotificationXml)
	{
		this.parse(sdmNotificationXml);
		
	}
	
	public boolean parse(String sdmNotificationXml)
	{
		boolean result = true;
		
        this.notificationXml = StringUtils.defaultString(sdmNotificationXml);
        
        // extract Modification type, either imei, or impuVR
		String notificationTypePlus = StringUtils.substringBetween(sdmNotificationXml, "name=", "modification=\"replace\">");
		this.notificationType = StringUtils.defaultString(StringUtils.substringBetween(notificationTypePlus, "\"", "\""));
        if (!(this.notificationType.matches("imei|impuVR")))
        {
        	logger.trace("Warning: couldn't determine if this was a imei or impuVR change: malformed SDM notification.   Couldn't find a line similar to <ns1:attribute name=\"imei\" modification=\"replace\">");
            result = false;
        }
        
        
        // extract Before value
        this.beforeValue = StringUtils.defaultString(StringUtils.substringBetween(sdmNotificationXml, ":beforeValue>", "<"));
        
        // extract After value
        this.afterValue = StringUtils.defaultString(StringUtils.substringBetween(sdmNotificationXml, ":afterValue>", "<"));
        
        
        // extract mdn value
        String mdnPlus = StringUtils.substringBetween(sdmNotificationXml, "attribute name=\"mdn\" modification=\"none\">", ":attribute>");
        this.mdn = StringUtils.defaultString(StringUtils.substringBetween(mdnPlus, ":currentValue>", "<"));
        if (mdn.equalsIgnoreCase(""))
        {
        	logger.trace("Warning: couldn't determine the mdn of this notification: malformed SDM notification.  Couldn't find a line similar to <ns1:attribute name=\"mdn\" modification=\"none\">");
            result = false;
        }
        
        // extract accountType value
        String accountTypePlus = StringUtils.substringBetween(sdmNotificationXml, "attribute name=\"accountType\" modification=\"none\">", ":attribute>");
        this.accountType = StringUtils.substringBetween(accountTypePlus, ":currentValue>", "<");
        
        // extract accountSubType value
        String accountSubTypePlus = StringUtils.substringBetween(sdmNotificationXml, "attribute name=\"accountSubType\" modification=\"none\">", ":attribute>");
        this.accountSubType = StringUtils.defaultString(StringUtils.substringBetween(accountSubTypePlus, ":currentValue>", "<"));
        
        
        // extract ou value
        String ouPlus = StringUtils.substringBetween(sdmNotificationXml, "attribute name=\"ou\" modification=\"none\">", ":attribute>");
        this.ou = StringUtils.defaultString(StringUtils.substringBetween(ouPlus, ":currentValue>", "<"));
        
        // extract subId value
        String subIdPlus = StringUtils.substringBetween(sdmNotificationXml, "attribute name=\"subId2\" modification=\"none\">", ":attribute>");
        this.subId2 = StringUtils.defaultString(StringUtils.substringBetween(subIdPlus, ":currentValue>", "<"));
        
        // extract ServiceName value
        this.serviceName = StringUtils.defaultString(StringUtils.substringBetween(sdmNotificationXml, "ServiceName>", "<"));
        
        // extract MsgId value
        this.msgId = StringUtils.defaultString(StringUtils.substringBetween(sdmNotificationXml, "MsgId>", "<"));
	
        // extract ConnId value
        this.connId = StringUtils.defaultString(StringUtils.substringBetween(sdmNotificationXml, "ConnId>", "<"));
        
        return result;
	}
	
	
	public String getNotificationType()
	{
		return notificationType;
	}

	public void setNotificationType(String notificationType)
	{
		this.notificationType = notificationType;
	}

	public String getBeforeValue()
	{
		return beforeValue;
	}

	public void setBeforeValue(String beforeValue)
	{
		this.beforeValue = beforeValue;
	}

	public String getAfterValue()
	{
		return afterValue;
	}

	public void setAfterValue(String afterValue)
	{
		this.afterValue = afterValue;
	}

	public String getMdn()
	{
		return mdn;
	}

	public void setMdn(String mdn)
	{
		this.mdn = mdn;
	}

	public String getAccountType()
	{
		return accountType;
	}

	public void setAccountType(String accountType)
	{
		this.accountType = accountType;
	}

	public String getAccountSubType()
	{
		return accountSubType;
	}

	public void setAccountSubType(String accountSubType)
	{
		this.accountSubType = accountSubType;
	}

	public String getOu()
	{
		return ou;
	}

	public void setOu(String ou)
	{
		this.ou = ou;
	}

	public String getSubId2()
	{
		return subId2;
	}

	public void setSubId2(String subId2)
	{
		this.subId2 = subId2;
	}
	
    public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getConnId() {
		return connId;
	}

	public void setConnId(String connId) {
		this.connId = connId;
	}
	
	
	public String getNotificationXml() {
		return notificationXml;
	}

	public void setNotificationXml(String notificationXml) {
		this.notificationXml = notificationXml;
	}
	
	@Override
	public String toString()
	{
		return ("notificationType = '" + notificationType + "'" +
				", beforeValue = '" + beforeValue + "'" + 
				", afterValue = '" + afterValue + "'" + 
				", mdn = '" + mdn + "'" + 
				", accountType = '" + accountType + "'" +
				", accountSubType = '" + accountSubType +"'" +
				", ou = '" + ou + "'" +
				", subId2 = '" + subId2 + "'" +
				", serviceName = '" + serviceName + "'" +
				", msgId = '" + msgId + "'" +
				", connId = '" + connId + "'" 
				);
	}
	

}

