## Architecture overview
A service which receives SDN Soap Notification messages as input and then sends Soap OTA requests in response.  Utimately this results in the subscribers 
sim card being updated.

### Technology stack
- JDK 7
- RestEasy (JAX-RS)
- StringTemplates
- R

## Developer setup
### Building
Clone this repository and build using the Gradle wrapper.

```
./gradlew clean build
```


### Running
- Download [JBoss EAP 6.3](https://www.jboss.org/products/eap/download/) and extract somewhere locally. Deploy the war file to standalone/deployments under the JBoss directory and if running JBoss should auto-deploy the war file.
=======
## Architecture overview
A service which receives SDM Soap Notification messages as input and then sends Soap OTA requests in response.  Utimately this results in the subscribers 
sim card being updated.

### Technology stack
- JDK 7
- RestEasy (JAX-RS)
- StringTemplates

## Developer setup
### Building
Clone this repository and build using the Gradle wrapper.

```
./gradlew clean build
```


### Running
- Download [JBoss EAP 6.3](https://www.jboss.org/products/eap/download/) and extract somewhere locally. Deploy the war file to standalone/deployments under the JBoss directory and if running JBoss should auto-deploy the war file.
- Alternatively (and if you have an IntelliJ Ultimate licence) you can use IntelliJ run configurations to manage JBoss from the IDE.