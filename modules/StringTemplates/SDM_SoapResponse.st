<?xml version='1.0' encoding='UTF-8' ?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header>
      <ns1:correlationHeader xmlns:ns1="http://new.webservice.namespace">
         <ns1:ServiceName>$serviceName$</ns1:ServiceName>
         <ns1:MsgId>$msgId$</ns1:MsgId>
         <ns1:ConnId>$connId$</ns1:ConnId>
      </ns1:correlationHeader>
   </soapenv:Header>
   <soapenv:Body>
      <ns4:NotificationResponse xmlns:ns4="http://new.webservice.namespace"> </ns4:NotificationResponse>
   </soapenv:Body>
</soapenv:Envelope>
