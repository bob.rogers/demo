<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://smarttrust.com/opm/schema/service" xmlns:com="http://smarttrust.com/opm/schema/common">
    <soapenv:Header>
        <wsse:Security soapenv:actor="http://schemas.xmlsoap.org/soap/actor/next" soapenv:mustUnderstand="0" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken>
                <wsse:Username>$otaUserName$</wsse:Username>
                <wsse:Password>$otaPassword$</wsse:Password>
            </wsse:UsernameToken>
        </wsse:Security>
    </soapenv:Header>
   <soapenv:Body>
      <ser:UpdateSubscriptionRequest>
         <ser:Msisdn>$mdn$</ser:Msisdn>

         <ser:ForceDownload>true</ser:ForceDownload>
      </ser:UpdateSubscriptionRequest>
   </soapenv:Body>
</soapenv:Envelope>